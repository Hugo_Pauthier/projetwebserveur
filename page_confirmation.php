<?php
require("BD/connect_bd.php");
require("static/navbar.php");

if (!isset($_POST['suppr_nomJeu'])) {
  // ajoute
  $n = $_POST['nom'];
  $a = "ajouté";
  // Test d'erreur sur les insertions 
  if (!insertJEUXVIDEO($_POST['nom'], $_POST['genre'],$_POST['nomimage']) || !insertDEVELOPPEUR($_POST['dev']) || !insertCREER($_POST['nom'], $_POST['dev'], $_POST['annee'])){
  ?>
    <!-- Affichage du message d'erreur -->
    <main role="main" class="container"><br><br>
      <div class="starter-template">
        <div class="alert alert-danger" role="alert">
          <h4 class="alert-heading">ERREUR !</h4>
          <p>Le jeu <?php echo $n ?> n'a pas été <?php echo $a ?></p>
          <a href="page_ajouter.php" class="btn btn-secondary my-2">Retour</a>
        </div>
      </div>
    </main>
    <?php
  }
  else {
?>
    <!-- Affichage du message de succès -->
    <main role="main" class="container"><br><br>
      <div class="starter-template">
        <div class="alert alert-success" role="alert">
          <h4 class="alert-heading">Ajouté !</h4>
          <p>Le jeu <?php echo $n ?> a bien été <?php echo $a ?></p>
          <a href="page_liste.php" class="btn btn-secondary my-2">Retour à la liste</a>
        </div>
      </div>
    </main>
  <?php
  }
}

else {
  // supprime
  $n = $_POST['suppr_nomJeu'];
  $a = "supprimé";
  deleteCREER($_POST['suppr_nomJeu']);
  deleteJEUXVIDEO($_POST['suppr_nomJeu']);
  ?>
  <!-- Affichage message de succès -->
  <main role="main" class="container"><br><br>
    <div class="starter-template">
      <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Supprimé !</h4>
        <p>Le jeu <?php echo $n ?> a bien été <?php echo $a ?></p>
        <a href="page_liste.php" class="btn btn-secondary my-2">Retour à la liste</a>
      </div>
     </div>
   </main>
  <?php
}
?>
