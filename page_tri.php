
<?php
require("BD/connect_bd.php");
require("static/navbar.php");
?>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column sideliste">
              <li class="nav-item">
                <a class="nav-link" href="page_liste.php">
                  Liste complète
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="page_tri.php">
                  Trier <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_ajouter.php">
                  Ajouter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_supprimer.php">
                  Supprimer
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="">
            <h1 class="h2">Trier</h1><br>
            <!-- Groupe de radios boutons -->
            <form class="" action="page_tri.php" method="GET">
              <div class="form-group">
                <div class="btn-group btn-group" data-toggle="buttons">
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="NomJeu"> Nom
                  </label>
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="NomDev"> Développeur
                  </label>
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="Annee"> Année
                  </label>
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="Genre"> Genre
                  </label>
                </div>
              </div>

              <?php
              if ((!isset($_GET['options']))) {
                echo "<p class=\"warning_liste\">Veuillez sélectionner une catégorie </p>";
              ?>
              <button type="submit" class="btn btn-outline-primary btn-block">Trier</button>
              <hr class="mb-4">
              <?php
              }
              else {
                ?>
                <br>
                <button type="submit" class="btn btn-outline-primary btn-block">Trier</button>
                <hr class="mb-4">
                <?php
                if ($_GET['options'] == "NomDev") {
                  $x = "Développeur";
                }
                elseif ($_GET['options'] == "NomJeu") {
                  $x = "Nom";
                }
                elseif ($_GET['options'] == "Annee") {
                  $x = "Année";
                }
                elseif ($_GET['options'] == "Genre") {
                  $x = "Genre";
                }
                echo "<h2 class = \"titre_liste\">Liste triée par ".$x."</h2><br>";

                // création des tableaux en fonction des radios sélectionnés
                if ($_GET['options'] == 'NomDev') {
                  createTableDev();
                }
                elseif ($_GET['options'] == 'Genre') {
                  createTableGenre();
                }
                elseif ($_GET['options'] == 'Annee') {
                  createTableAnnee();
                }
                elseif ($_GET['options'] == 'NomJeu') {
                  createTableJeu();
                }
              }
                ?>
                </form>
              </div><br>
        </main>
      </div>
    </div>
  </body>
</html>
