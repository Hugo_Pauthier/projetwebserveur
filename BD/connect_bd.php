<?php
require_once("connect.php");

// Permet de se connecter à la base de données
// Retourne un objet PDO
function connectBD() {
  $dsn = "mysql:dbname=".BASE.";host=".SERVER;
  try{
    $connexion = new PDO($dsn, USER, PASSWD);
  }
  catch(PDOException $e){
    printf("Echec de la connexion : %s\n", $e->getMessage());
    exit();
  }
  return $connexion;
}

// Permet d'insérer un Jeu dans la table JEUXVIDEO
// Retourne True en cas de succès ou False si une erreur survient
function insertJEUXVIDEO($nom,$genre,$nomimage) {
  $bdd = connectBD();
  $sql4="INSERT into JEUXVIDEO values (:nom,:genre,:nomimage)";
  $stmt4=$bdd->prepare($sql4);
  $stmt4->bindParam(':nom', $nom);
  $stmt4->bindParam(':genre', $genre);
  $stmt4->bindParam(':nomimage', $nomimage);
  if ($stmt4->execute()) {
    return True;
  }
  return False;
}

// Permet d'insérer un Développeur dans la table DEVELOPPEUR
// Retourne True en cas de succès ou False si une erreur survient
function insertDEVELOPPEUR($nomDev) {
  $bdd = connectBD();
  $sql5="INSERT IGNORE into DEVELOPPEUR values (:dev)";
  $stmt5=$bdd->prepare($sql5);
  $stmt5->bindParam(':dev', $nomDev);
  if ($stmt5->execute()) {
    return True;
  }
  return False;
}

// Permet d'insérer une relation Jeux x Développeur x Année dans la table CREER
// Retourne True en cas de succès ou False si une erreur survient
function insertCREER($nomJeu, $nomDev, $annee) {
  $bdd = connectBD();
  $sql6="INSERT into CREER values (:nom, :dev, :annee)";
  $stmt6=$bdd->prepare($sql6);
  $stmt6->bindParam(':nom', $nomJeu);
  $stmt6->bindParam(':dev', $nomDev);
  $stmt6->bindParam(':annee', $annee);
  if ($stmt6->execute()) {
    return True;
  }
  return False;
}

// Permet de supprimer un Jeu de la table JEUXVIDEO
// Retourne True en cas de succès ou False si une erreur survient
function deleteJEUXVIDEO($nom) {
  $bdd = connectBD();
  $sql3="DELETE from JEUXVIDEO where NomJeu =:supprnomJeu";
  $stmt3=$bdd->prepare($sql3);
  $stmt3->bindParam(':supprnomJeu', $nom);
  if ($stmt3->execute()) {
    return True;
  }
  return False;
}

// Permet de supprimer une ligne de la table CREER
// Retourne True en cas de succès ou False si une erreur survient
function deleteCREER($nom) {
  $bdd = connectBD();
  $sql1="DELETE from CREER where NomJeu =:supprnomJeu";
  $stmt1=$bdd->prepare($sql1);
  $stmt1->bindParam(':supprnomJeu', $nom);
  if ($stmt1->execute()) {
    return True;
  }
  return False;
}

// Affiche un tableau par developpeur
function createTableDev() {
  $sql7 = "select distinct NomDev from JEUXVIDEO natural join DEVELOPPEUR natural join CREER order by NomDev";
  $bdd = connectBD();
  if(!$bdd->query($sql7)) echo "Pb d'accès à la base";
  else {
    foreach ($bdd->query($sql7) as $nom) {
    echo "
    <h3>".$nom['NomDev']."</h3>
    <div class=\"tabl\">
      <table class=\"table table-striped table-bordered \">
        <thead class=\"thead\">
          <tr>
            <th scope=\"col\">Nom</th>

            <th scope=\"col\">Genre</th>
            <th scope=\"col\" class=\"cell1\">Année</th>
          </tr>
        </thead>
        <tbody>
    ";
    $sql8 = "select NomJeu,NomDev,Genre,Annee from JEUXVIDEO natural join DEVELOPPEUR natural join CREER where NomDev='".$nom['NomDev']."'";
    if(!$bdd->query($sql8)) echo "Pb d'accès à la base";
    else {
      foreach ($bdd->query($sql8) as $row)
      echo "
      <tr>
        <td class = \"c\"><a href=\"action_page_jeu.php?nom_page=".$row['NomJeu']."\">".$row['NomJeu']."</a></td>

        <td>".$row['Genre']."</td>
        <td>".$row['Annee']."</td>
      </tr>
      ";
    }
    echo "</tbody> </table> </div> <br>";
    }
  }
}

// Affiche un tableau par genre
function createTableGenre() {
  $sql9 = "select distinct Genre from JEUXVIDEO natural join DEVELOPPEUR natural join CREER order by Genre";
  $bdd = connectBD();
  if(!$bdd->query($sql9)) echo "Pb d'accès à la base";
  else {
    foreach ($bdd->query($sql9) as $genre) {
    echo "
    <h3>".$genre['Genre']."</h3>
    <div class=\"tabl\">
      <table class=\"table table-striped table-bordered \">
        <thead class=\"thead\">
          <tr>
            <th scope=\"col\">Nom</th>
            <th scope=\"col\" class=\"\">Développeur</th>
            <th scope=\"col\" class=\"cell1\">Année</th>
          </tr>
        </thead>
        <tbody>
    ";
    $sql10 = "select NomJeu,NomDev,Genre,Annee from JEUXVIDEO natural join DEVELOPPEUR natural join CREER where Genre='".$genre['Genre']."'";
    if(!$bdd->query($sql10)) echo "Pb d'accès à la base";
    else {
      foreach ($bdd->query($sql10) as $row)
      echo "
      <tr>
        <td class = \"c\"><a href=\"action_page_jeu.php?nom_page=".$row['NomJeu']."\">".$row['NomJeu']."</a></td>
        <td class = \"l\">".$row['NomDev']."</td>
        <td>".$row['Annee']."</td>
      </tr>
      ";
    }
    echo "</tbody> </table> </div> <br>";
    }
  }
}

// Affiche un tableau par année
function createTableAnnee() {
  $sql11 = "select distinct Annee from JEUXVIDEO natural join DEVELOPPEUR natural join CREER order by Annee desc";
  $bdd = connectBD();
  if(!$bdd->query($sql11)) echo "Pb d'accès à la base";
  else {
    foreach ($bdd->query($sql11) as $annee) {
    echo "
    <h3>".$annee['Annee']."</h3>
    <div class=\"tabl\">
      <table class=\"table table-striped table-bordered \">
        <thead class=\"thead\">
          <tr>
            <th scope=\"col\">Nom</th>
            <th scope=\"col\" class=\"\">Développeur</th>
            <th scope=\"col\">Genre</th>
          </tr>
        </thead>
        <tbody>
    ";
    $sql12 = "select NomJeu,NomDev,Genre,Annee from JEUXVIDEO natural join DEVELOPPEUR natural join CREER where Annee='".$annee['Annee']."'";
    if(!$bdd->query($sql12)) echo "Pb d'accès à la base";
    else {
      foreach ($bdd->query($sql12) as $row)
      echo "
      <tr>
        <td class = \"c\"><a href=\"action_page_jeu.php?nom_page=".$row['NomJeu']."\">".$row['NomJeu']."</a></td>
        <td class = \"l\">".$row['NomDev']."</td>
        <td>".$row['Genre']."</td>
      </tr>
      ";
    }
    echo "</tbody> </table> </div> <br>";
    }
  }
}

// Afficher une liste des jeux sous forme d'un tableau
function createTableJeu() {
  echo "
  <table class=\"table table-bordered table-striped tabl\">
  <thead>
    <tr>
      <th scope=\"col\">Nom</th>
      <th scope=\"col\" class=\"\">Développeur</th>
      <th scope=\"col\">Genre</th>
      <th scope=\"col\" class=\"cell1\">Année</th>
    </tr>
  </thead>
  <tbody>";

    $sql = "select NomJeu,NomDev,Genre,Annee from JEUXVIDEO natural join DEVELOPPEUR natural join CREER order by ".$_GET['options'];
    $bdd = connectBD();
    if(!$bdd->query($sql)) echo "Pb d'accès à la base";
    else {
      foreach ($bdd->query($sql) as $row)
      echo "
      <tr>
      <td class = \"c\"><a href=\"action_page_jeu.php?nom_page=".$row['NomJeu']."\">".$row['NomJeu']."</a></td>
      <td class = \"l\">".$row['NomDev']."</td>
      <td>".$row['Genre']."</td>
      <td>".$row['Annee']."</td>
      </tr>
      ";
    }
}

?>
