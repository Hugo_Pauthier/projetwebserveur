DROP TABLE CREER;
DROP TABLE DEVELOPPEUR;
DROP TABLE JEUXVIDEO;

CREATE TABLE `JEUXVIDEO`(
  `NomJeu` varchar(30) DEFAULT NULL,
  `Genre` varchar(30) DEFAULT NULL,
  `Image` text DEFAULT NULL,
  PRIMARY KEY (`NomJeu`)
);

CREATE TABLE `DEVELOPPEUR`(
  `NomDev` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`NomDev`)
);

CREATE TABLE `CREER`(
  `NomJeu` varchar(30),
  `NomDev` varchar(30),
  `Annee` varchar(4),
  PRIMARY KEY(`NomJeu`,`NomDev`),
  FOREIGN KEY(`NomJeu`) REFERENCES JEUXVIDEO(`NomJeu`),
  FOREIGN KEY(`NomDev`) REFERENCES DEVELOPPEUR(`NomDev`)
);

)
INSERT INTO JEUXVIDEO VALUES
('Super Mario Galaxy','Plateforme','fiche_super_mario_galaxy.jpg'),
('League of Legends','MOBA','fiche_LoL.jpg'),
('Dofus','MMORPG','fiche_dofus.jpg'),
('World of Warcraft','MMORPG','fiche_wow.jpg'),
('Hearthstone','Cartes','fiche_hearthstone.jpg'),
('Fortnite','Battle Royale','fiche_fortnite.jpg'),
('Overwatch','FPS','fiche_overwatch.jpg'),
('PlayersUnknown\'s Battlegrounds','Battle Royale','fiche_PUBG.jpg');


INSERT INTO DEVELOPPEUR VALUES
('Nintendo'),
('Blizzard'),
('Ankama'),
('Riot Games'),
('Epic Games'),
('Bluehole');

INSERT INTO CREER VALUES
('Super Mario Galaxy','Nintendo','2007'),
('League of Legends','Riot Games','2009'),
('Dofus','Ankama','2004'),
('World of Warcraft','Blizzard','2004'),
('Hearthstone','Blizzard','2014'),
('Fortnite','Epic Games','2017'),
('Overwatch','Blizzard','2016'),
('PlayersUnknown\'s Battlegrounds','Bluehole','2017');
