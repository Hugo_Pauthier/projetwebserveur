<?php
require("BD/connect_bd.php");
require("static/navbar.php");
?>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column sideliste">
          <li class="nav-item">
            <a class="nav-link" href="page_liste.php">
              Liste complète <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page_tri.php">
              Trier
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page_ajouter.php">
              Ajouter
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="page_supprimer.php">
              Supprimer
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <h4 class="h2">Supprimer un jeu</h4><br>
          <form class="needs-validation" action="page_confirmation.php" method="post">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="nomJeu">Nom du jeu</label>
                <!-- Créer une liste déroulante avec tous les jeux de la base -->
                <select class="form-control" name="suppr_nomJeu">
                  <?php
                  $sql = "select * from CREER";
                  $bdd = connectBD();
                  if(!$bdd->query($sql)) echo "Pb d'accès à la base";
                  else {
                    foreach ($bdd->query($sql) as $row)
                    echo "<option>".$row['NomJeu']."</option>";
                        }
                  ?>
                </select>
              </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-outline-primary btn-block" type="submit">Confirmer</button>
          </form><br>
        </main>
      </div>
    </div>
  </body>
</html>
