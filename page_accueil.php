<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Site Jeux Vidéo</title>

    <!-- CSS Boostrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- custom CSS -->
    <link href="css/style4.css" rel="stylesheet">
  </head>
  <body class ="bodyaccueil">

    <nav class="navbar navbar-expand-md navbar-light navbar-bg fixed-top flex-md-nowrap shadow">
      <a class="navbar-brand" href="page_accueil.php">
        <img src="icon/gamepad-console_icon.png" width="30" height="30" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_accueil.php">Accueil <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page_liste.php">Liste des Jeux</a>
          </li>
        </ul>
      </div>
    </nav>
    <main role="main" class="container text-center">
      <div class="">
        <h1>Accueil</h1><br>
        <p class="">Salut tout le monde !<br><br>
          Ici vous trouverez une liste de jeux vidéo répertoriés par nom.<br>
          Vous pourrez aussi retrouver le développeur du jeu et l'année de sortie du jeu.
        <br><br>
        </p>
      </div>
    </main>
  </body>
</html>
