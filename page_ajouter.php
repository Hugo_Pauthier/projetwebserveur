<?php
require("BD/connect_bd.php");
require("static/navbar.php");
?>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column sideliste">
          <li class="nav-item">
            <a class="nav-link" href="page_liste.php">
              Liste complète <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page_tri.php">
              Trier
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="page_ajouter.php">
              Ajouter
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page_supprimer.php">
              Supprimer
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <h4 class="h2">Ajouter un jeu</h4><br>
          <form class="needs-validation" action="page_confirmation.php" method="post">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" id="firstName" placeholder="" value="" name="nom" required>
              </div>
              <div class="col-md-6 mb-3">
                <label for="developpeur">Développeur</label>
                <input type="text" class="form-control" placeholder="" value="" name="dev" required>
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-4  mb-3">
                <label for="genre">Genre</label>
                <input type="text" class="form-control" placeholder="" value="" name="genre" required>
              </div>
              <div class="col-md-4 mb-3">
                <label for="annee">Année de sortie</label>
                <input class="form-control" type="number" min="1950" max="2019" name="annee" required/>
              </div>
              <div class="col-md-4 mb-3">
                <label for="url">URL de l'image</label>
                <input type="text" class="form-control" placeholder="" name="nomimage" required>
              </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-outline-primary btn-block" type="submit">Confirmer</button>
          </form><br>
        </main>
      </div>
    </div>
  </body>
</html>
